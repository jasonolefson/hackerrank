bits = [False, True, False, False, True, False, False, True]
new_bits = []

for b in bits:
    if b == True:
        new_bits.append(1)
    elif b == False:
        new_bits.append(0)

super_bits = [1 if b == True else 0 for b in bits]

print(new_bits)
print(super_bits)

