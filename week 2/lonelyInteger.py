# Given an array of integers, where all elements but one occur twice, find the unique element.

# Example
# a = [1,2,3,4,3,2,1]

# The unique element is 4.

# Function Description

# Complete the lonelyinteger function in the editor below.

# lonelyinteger has the following parameter(s):

# int a[n]: an array of integers
# Returns

# int: the element that occurs only once
# Input Format

# The first line contains a single integer, , the number of integers in the array.
# The second line contains  space-separated integers that describe the values in .
from collections import Counter
def lonelyinteger(a):
    # Write your code here
    count = 0
    target = 0
    for num in a:
        if count == 1:
            return target
        for x in a:
            if num == x:
                count += 1
            if count == 1:
                target = num
            elif count == 2:
                target = 0
                count = 0


print(lonelyinteger([1,1,2,4,3,2]))

# OR

def lonelyinteger(a):
    count = Counter(a)
    for key, value in count.items():
        if value == 1:
            return key