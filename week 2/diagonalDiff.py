def diagonalDifference(arr):
     # Write your code here
     diagonal = [arr[i][i] for i in range(len(arr))]
     diag_count = [arr[i][~i] for i in range(len(arr))]
     res = sum(diagonal) - sum(diag_count)
     return abs(res)

    


print(diagonalDifference([5, [1, 2, 3], [4, 5, 6], [7, 8, 9]]))
print(diagonalDifference([4, [1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]))