def gradingStudents(grades):
    lst=[]
    # Write your code here
    for x in grades:
        if x < 38:
            lst.append(x)
        elif (x + 1) % 5 == 0:
            lst.append(x + 1)
        elif (x + 2) % 5 == 0:
            lst.append(x + 2)
        else:
            lst.append(x)
    return lst

print(gradingStudents([4,73,67,38,33]))