import random
from collections import Counter

obj_list = ["A", "B", "C", "D", "E"]
# without utilizing counter below
# dct = {}

# for obj in obj_list:
#     dct[obj] = 0


# for _ in range(100):
#     recv_obj = random.choice(obj_list)
#     dct[recv_obj] += 1

# print(dct)


# using counter
counter = Counter()
for _ in range(100):
    recv_obj = random.choice(obj_list)
    counter[recv_obj] += 1
print(counter)
print(counter.total())