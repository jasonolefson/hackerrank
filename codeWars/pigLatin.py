# Move the first letter of each word to the end of it, then add "ay" to the end of the word. Leave punctuation marks untouched.

# Examples
# pig_it('Pig latin is cool') # igPay atinlay siay oolcay
# pig_it('Hello world !')     # elloHay orldway !

def pig_it(text):
    pig = "ay"
    split = text.split(" ")
    lst = []
    for x in split:
        if x == "?" or x == "!" or x == ".":
            lst.append(x)
            continue
        lst.append(x[1:] + x[0] + pig)
    string = ' '.join(lst)
    return string

# print(pig_it('Pig latin is cool'))
# print(pig_it('This is my string'))
print(pig_it("Quis custodiet ipsos custodes ?"))


# using list comprehention
def pig_it(text):
    lst = text.split()
    return ' '.join( [word[1:] + word[:1] + 'ay' if word.isalpha() else word for word in lst])