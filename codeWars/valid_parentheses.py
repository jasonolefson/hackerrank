# # Write a function that takes a string of parentheses, and determines
#  if the order of the parentheses is valid. The function should return true if the string is valid, and false if it's invalid.

# # Examples
# # "()"              =>  true
# # ")(()))"          =>  false
# # "("               =>  false
# # "(())((()())())"  =>  true
# # Constraints
# # 0 <= input.length <= 100

# # Along with opening (() and closing ()) parenthesis, input may contain any
# #  valid ASCII characters. Furthermore, the input string may be empty and/or
# #  not contain any parentheses at all. Do not treat other forms of brackets as parentheses (e.g. [], {}, <>).

# attempt 1
# def valid_parentheses(string):
#     split = [*string]
#     count = 0
#     if string == "":
#         return True
#     elif split[0] == ")":
#         return False
#     elif split[-1] == "(":
#         return False
#     for x in split:
#         if x == "(":
#             count += 1
#         elif x == ")":
#             count -= 1
#     if count == 0:
#         return True
#     else:
#         return False
    
# attempt 2 PASS
def valid_parentheses(string):
    stack = []
    for i in string:
        if i.isalpha():
            continue
        if i == "(":
            stack.append(i)
            continue
        if len(stack) == 0:
            return False
        stack.pop()
    return True if len(stack) == 0 else False

#attempt 3 PASS
def valid_parentheses(string):
    cnt = 0
    for char in string:
        if char == '(': cnt += 1
        if char == ')': cnt -= 1
        if cnt < 0: return False
    return True if cnt == 0 else False

    
# print(valid_parentheses("()"))
# expect true
# print(valid_parentheses(")(()))"))
# expect false
print(valid_parentheses("noukm(kmx)ft(oq)fbyv)p(wbmmtg"))
# expect false
# print(valid_parentheses("(())((()())())"))
# expect true