# Write a function that takes a positive integer and returns the next
# smaller positive integer containing the same digits.

# For example:

# next_smaller(21) == 12
# next_smaller(531) == 513
# next_smaller(2071) == 2017
# Return -1 when there is no smaller number that contains the same digits.
# Also return -1 when the next smaller number with the same digits would require the leading digit to be zero.

# next_smaller(9) == -1
# next_smaller(135) == -1
# next_smaller(1027) == -1  # 0721 is out since we don't write numbers with leading zeros
# some tests will include very large numbers.
# test data only employs positive integers.

def next_smaller(n):
    # create a list of int's through list comprehention
    lst = [int(i) for i in str(n)]
    pos = len(lst)-1
    nxt_small_counter = nxt_small = float("-inf")
    nxt_small = float("-inf")
    # error handling
    if(len(lst) == 1 or lst == sorted(lst)):
        return -1
    # iterate through lst to find the first number > prev digit
    for i in range(len(lst)-1,0,-1):
        if(lst[i] < lst[i-1]):
            pos -= 1
            break
        pos -= 1
    # iterate from position to the end of list for next smallest number
    for i in range(pos, len(lst)):
        if(lst[i] < lst[pos] and lst[i] > nxt_small):
            nxt_small = lst[i]
            nxt_small_counter = i
    # swap numbers
    lst[pos], lst[nxt_small_counter] = lst[nxt_small_counter], lst[pos]
    # sort idx to end of list
    lst2 = lst[pos+1:]
    lst2.sort(reverse=True)
    # replace ints in list
    for i in range(len(lst2)):
        lst[i+pos+1] = lst2[i]
    lst = [str(x) for x in lst]
    s = ''.join(lst)
    return int(s) if s[0] != '0' else -1

print(next_smaller(907))
# expected: 790
print(next_smaller(531))
# expected: 513
print(next_smaller(135))
# expected: -1
print(next_smaller(1234567908))
# expected: 1234567890