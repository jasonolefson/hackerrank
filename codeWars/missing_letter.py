# Write a method that takes an array of consecutive (increasing)
# letters as input and that returns the missing letter in the array.

# # You will always get an valid array. And it will be always
#  exactly one letter be missing. The length of the array will always be at least 2.
# The array will always contain letters in only one case.

# Example:

#  ['a','b','c','d','f'] -> 'e'
#  ['O','Q','R','S'] -> 'P'
#  (Use the English alphabet with 26 letters!)

# Have fun coding it and please don't forget to vote and rank this kata! :-)

def find_missing_letter(chars):
    # declare a variable to a string of the alphabet
    alph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    # create an idx to search for char
    idx = alph.find(chars[0])
    comp = alph[idx: idx + len(chars) + 1]

    # use enumerate in for loop
    for x, y in enumerate(comp):
        if chars[x] != comp[x]:
            return comp[x]
        
# method 2
def find_missing_letter(chars):
    # declare a counter variable to keep track of iterations
    n = 0
    # while the unicode number for char[n] is equal to the unicode number for char[n + 1] *[the next character] subtracted by 1,
    # meaning a -> b would be 97 == 98 - 1........... which would be true 
    while ord(chars[n]) == ord(chars[n+1]) - 1:
        # add one to n.....and continue the loop checking the next char for comparison
        n += 1
    return chr(1+ord(chars[n]))

print(find_missing_letter(['a','b','c','d','f']))
print(find_missing_letter(['O','Q','R','S']))