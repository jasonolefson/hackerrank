# Build a pyramid-shaped tower, as an array/list of strings, given a 
# positive integer number of floors. A tower block is represented with "*" character.

# For example, a tower with 3 floors looks like this:

# [
#   "  *  ",
#   " *** ", 
#   "*****"
# ]
# And a tower with 6 floors looks like this:

# [
#   "     *     ", 
#   "    ***    ", 
#   "   *****   ", 
#   "  *******  ", 
#   " ********* ", 
#   "***********"
# ]

# attempt 1
# problem: missing empty spaces
def tower_builder(n_floors):
    count = 1
    brick = "*"
    tower = ["*"]
    # build here
    if n_floors == 1:
        return tower
    else:
        while count <= n_floors:
            count += 2
            brick += "**"
            tower.append(brick)
#     return tower

# attempt 2
# problem: I was able to make the empty spaces, but unable to insert stars
# def tower_builder(n_floors):
#     count = 1
#     tower = []
#     # make the "boxes" of the tower
#     while count <= n_floors:
#         count += 1
#         tower.append(" " * n_floors)
#     for i in tower:
#         length = len(i)
#         middle = length // 2
#         l = list(i)
#         l[middle] = "*"
#         "".join(l)
#     return tower

#attempt 3
def tower_builder(n_floors):
    tower = []
    width = (n_floors * 2) - 1
    for x in range(1, n_floors * 2, 2):
        stars = x * "*"
        line = stars.center(width)
        tower.append(line)
    return tower

        

print(tower_builder(1))
print(tower_builder(2))
print(tower_builder(3))